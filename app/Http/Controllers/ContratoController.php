<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contrato;

class ContratoController extends Controller
{
    /**
     * Regresa el listado de contratos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Contrato::all();
    }

    /**
     * Registra un nuevo contrato
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contrato = new Contrato;
        $contrato->nombre= $request['nombre'];
        $contrato->descripcion= $request['descripcion'];    
        $contrato->save();        
        return $contrato;
    }
    /**
     * Consulta un contrato dado su id y retorna la información de este
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Contrato::find($id);
    }

    /**
     * Actualiza la información de un contrato dado su id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contrato = Contrato::find($id);  
        //si exite el name del request lo pone, si no pone el nombre de la actividad.
        $contrato->nombre = isset($request['nombre']) ? $request['nombre'] : $contrato->nombre; 
        $contrato->descripcion = isset($request['descripcion']) ? $request['descripcion'] : $contrato->descripcion;
        $contrato->save();        
        return $contrato;
    }

    /**
     * Elimina el registro de un contrato dado su id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contrato = Contrato::find($id);
        $nombre = $contrato->nombre;
        $contrato->delete();

        return "Contrato '{$nombre}' eliminado";
    }
}
