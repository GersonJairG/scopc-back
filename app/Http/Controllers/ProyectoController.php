<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proyecto;

class ProyectoController extends Controller
{
    /**
     * Regresa el listado de proyectos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Proyecto::all();
    }

    /**
     * Registra un nuevo proyecto
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proyecto = new Proyecto;
        $proyecto->nombre = $request['nombre'];
        $proyecto->fecha_inicio = $request['fechaInicio'];
        $proyecto->departamento = $request['departamento'];
        $proyecto->ciudad = $request['ciudad'];
        // $proyecto->avance_parcial = $request['avanceParcial'];
        $proyecto->avance_parcial = 0;
        $proyecto->id_contrato = $request['idContrato'];
        $proyecto->save();        
        return $proyecto;
    }
    /**
     * Consulta un proyecto dado su id y retorna la información de este
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Proyecto::find($id);
    }

    /**
     * Actualiza la información de un proyecto dado su id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proyecto = Proyecto::find($id);  
        //si exite el name del request lo pone, si no pone el nombre del proyecto.
        $proyecto->nombre = isset($request['nombre']) ? $request['nombre'] : $proyecto->nombre; 
        $proyecto->fecha_inicio = isset($request['fechaInicio']) ? $request['fechaInicio'] : $proyecto->fecha_inicio;
        $proyecto->ubicacion = isset($request['ubicacion']) ? $request['ubicacion'] : $proyecto->ubicacion;
        $proyecto->avance_parcial = isset($request['avanceParcial']) ? $request['avanceParcial'] : $proyecto->avance_parcial;
        $proyecto->id_contrato = isset($request['idContrato']) ? $request['idContrato'] : $proyecto->id_contrato;
        $proyecto->save();        
        return $proyecto;
    }

    /**
     * Elimina el registro de un proyecto dado su id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proyecto = Proyecto::find($id);
        $nombre = $proyecto->nombre;
        $proyecto->delete();

        return "Proyecto '{$nombre}' eliminado";
    }
}
