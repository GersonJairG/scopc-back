<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Actividad;

class ActividadController extends Controller
{
    /**
     * Regresa el listado de actividades
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Actividad::all();
    }

    /**
     * Registra una nueva actividad
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $actividad = new Actividad;
        $actividad->nombre= $request['nombre'];
        $actividad->unidad= $request['unidad'];
        $actividad->precio= $request['precio'];
        $actividad->id_tipo_actividad= $request['idTipoActividad'];
        $actividad->id_proyecto= $request['idProyecto'];
        $actividad->save();        
        return $actividad;
    }
    /**
     * Consulta una actividad dado su id y retorna la información de esta
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Actividad::find($id);
    }

    /**
     * Actualiza la información de una actividad dado su id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $actividad = Actividad::find($id);  
        //si exite el name del request lo pone, si no pone el nombre de la actividad.
        $actividad->nombre = isset($request['nombre']) ? $request['nombre'] : $actividad->nombre; 
        $actividad->unidad = isset($request['unidad']) ? $request['unidad'] : $actividad->unidad;
        $actividad->precio = isset($request['precio']) ? $request['precio'] : $actividad->precio;
        $actividad->id_tipo_actividad = isset($request['idTipoActividad']) ? $request['idTipoActividad'] : $actividad->id_tipo_actividad;
        $actividad->id_proyecto = isset($request['idProyecto']) ? $request['idProyecto'] : $actividad->id_proyecto;
        $actividad->save();        
        return $actividad;
    }

    /**
     * Elimina el registro de una actividad dado su id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $actividad = Actividad::find($id);
        $nombre = $actividad->nombre;
        $actividad->delete();

        return "Actividad '{$nombre}' eliminada";
    }
}
