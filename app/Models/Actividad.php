<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 * @property string $unidad
 * @property string $precio
 * @property int $id_tipo_actividad
 * @property int $id_proyecto
 */
class Actividad extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'actividad';
    protected $hidden = ['pivot'];

    /**
     * @var array
     */
    protected $fillable = [
        'nombre', 'unidad', 'precio', 'id_tipo_actividad', 'id_proyecto'
    ];

    //belongsTo de tipo_actividad
    public function tiposActividades(){
        return $this->belongsTo('App\Models\TipoActividad');
     }

     //belongsTo de proyecto
    public function proyectos(){
        return $this->belongsTo('App\Models\Proyecto');
     }

    //relacion uno a muchos con recurso_mano_de_obra
    public function recursosManoDeObra(){
        return $this->hasMany('App\Models\RecursoManoDeObra', 'id_actividad');
    }

    //relacion uno a muchos con recurso_maquinaria
    public function recursosMaquinaria(){
        return $this->hasMany('App\Models\RecursoMaquinaria', 'id_actividad');
    }

    //relacion uno a muchos con recurso_material
    public function recursosMaterial(){
        return $this->hasMany('App\Models\RecursoMaterial', 'id_actividad');
    }



}
