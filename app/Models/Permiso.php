<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $nombre
 * @property string $descripcion
 */
class Permiso extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'permiso';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion'];
    
    //relacion uno a muchos con rol_permiso
    public function rolesPermisos(){
        return $this->hasMany('App\Models\RolPermiso', 'id_permiso');
    }
}
