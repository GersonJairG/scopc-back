<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_actividad
 * @property int $id_usuario
 */
class RolPermiso extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'rol_permiso';
    protected $hidden = ['pivot'];

    /**
     * @var array
     */
    protected $fillable = ['id_rol', 'id_permiso'];

    //belongsTo de rol
    public function roles(){
        return $this->belongsTo('App\Models\Rol');
     }

    //belongsTo de permiso
    public function permisos(){
        return $this->belongsTo('App\Models\Permiso');
     }

}
