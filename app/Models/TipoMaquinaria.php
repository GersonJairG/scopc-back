<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 */
class TipoMaquinaria extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_maquinaria';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion'];

    //relacion uno a muchos con recurso_maquinaria
    public function recursos(){
        return $this->hasMany('App\Models\RecursoMaquinaria', 'id_tipo_maquinaria');
    }

}
