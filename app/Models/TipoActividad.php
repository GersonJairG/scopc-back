<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 */
class TipoActividad extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_actividad';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion'];

    //relacion uno a muchos con actividad
    public function actividades(){
        return $this->hasMany('App\Models\Actividad', 'id_tipo_actividad');
    }

}
