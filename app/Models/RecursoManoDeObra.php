<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_actividad
 * @property string $descripcion
 * @property string $unidad
 * @property float $cantidad
 * @property float $valor_unitario
 * @property float $valor_parcial
 */
class RecursoManoDeObra extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'recurso_mano_de_obra';

    /**
     * @var array
     */
    protected $fillable = ['encargado', 'funcion', 'experiencia', 'salario', 'unidad', 'rendimiento','id_actividad','id_tipo_mano_de_obra'];

    //belongsTo de actividad
    public function actividades(){
        return $this->belongsTo('App\Models\Actividad');
     }

    //belongsTo de tipo_mano_de_obra
    public function tiposManoDeObra(){
        return $this->belongsTo('App\Models\TipoManoDeObra');
     }
}
