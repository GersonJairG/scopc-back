<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_actividad
 * @property int $id_usuario
 */
class ProyectoUsuario extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'proyecto_usuario';
    protected $hidden = ['pivot'];

    /**
     * @var array
     */
    protected $fillable = ['id_proyecto', 'id_usuario','id_rol'];

    //belongsTo de proyecto
    public function proyectos(){
        return $this->belongsTo('App\Models\Proyecto');
     }

    //belongsTo de usuario
    public function usuarios(){
        return $this->belongsTo('App\Models\User');
     }

      //belongsTo de roles
    public function roles(){
        return $this->belongsTo('App\Models\Rol');
     }

}
