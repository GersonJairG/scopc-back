<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $nombre
 * @property string $descripcion
 */
class Estado extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'estado';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion'];

    //relacion uno a muchos con usuario
    public function usuarios(){
        return $this->hasMany('App\Models\Estado', 'id_estado');
    }

}
