<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 * @property date $fecha_inicio
 * @property string $ubicacion
 * @property string $avance_parcial
 * @property string $persona_encargada
 * @property int $id_contrato
 */
class Proyecto extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'proyecto';
    protected $hidden = ['pivot'];

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'fecha_inicio', 'ciudad', 'departamento', 'avance_parcial', 'id_contrato'];

    //relacion uno a muchos con actividad
    public function actividades(){
        return $this->hasMany('App\Models\Actividad', 'id_proyecto');
    }

    //relacion uno a muchos con proyecto_usuario
    public function proyectosUsuario(){
        return $this->hasMany('App\Models\ProyectoUsuario', 'id_proyecto');
    }

    //belongsTo de contrato
    public function contratos(){
        return $this->belongsTo('App\Models\Contrato');
     }



}
