<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_actividad
 * @property string $descripcion
 * @property string $unidad
 * @property float $cantidad
 * @property float $valor_unitario
 * @property float $valor_parcial
 */
class RecursoMaquinaria extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'recurso_maquinaria';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'precio','id_actividad','id_tipo_maquinaria'];

    //belongsTo de actividad
    public function actividades(){
        return $this->belongsTo('App\Models\Actividad');
     }

    //belongsTo de tipo_maquinaria
    public function tiposMaquinarias(){
        return $this->belongsTo('App\Models\TipoMaquinaria');
     }

}
