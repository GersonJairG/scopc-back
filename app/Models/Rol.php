<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $nombre
 * @property string $descripcion
 */
class Rol extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'rol';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion'];

    //relacion uno a muchos con usuario
    // public function usuarios(){
    //     return $this->hasMany('App\Models\User', 'id_rol');
    // }

     //relacion uno a muchos con rol_permiso
     public function rolesPermisos(){
        return $this->hasMany('App\Models\RolPermiso', 'id_rol');
    }

    //relacion uno a muchos con proyecto_usuario
    public function proyectoUsuario(){
        return $this->hasMany('App\Models\ProyectoUsuario', 'id_rol');
    }

}
