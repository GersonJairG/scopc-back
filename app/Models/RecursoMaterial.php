<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_actividad
 * @property string $descripcion
 * @property string $unidad
 * @property float $cantidad
 * @property float $valor_unitario
 * @property float $valor_parcial
 */
class RecursoMaterial extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'recurso_material';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'medida', 'peso', 'precio','id_actividad','id_tipo_material'];

    //belongsTo de actividad
    public function actividades(){
        return $this->belongsTo('App\Models\Actividad');
     }

    //belongsTo de tipo_material
    public function tiposMateriales(){
        return $this->belongsTo('App\Models\TipoMaterial');
     }

}
