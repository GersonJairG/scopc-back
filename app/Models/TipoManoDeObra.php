<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 */
class TipoManoDeObra extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_mano_de_obra';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion'];

    //relacion uno a muchos con recurso
    public function recursos(){
        return $this->hasMany('App\Models\TipoManoDeObra', 'id_tipo_mano_de_obra');
    }

}
