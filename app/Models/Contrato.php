<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 */
class Contrato extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'contrato';

    /**
     * @var array
     */
    protected $fillable = ['nombre','descripcion'];

    //relacion uno a muchos con proyecto
    public function proyectos(){
        return $this->hasMany('App\Models\Proyecto', 'id_contrato');
    }

}
