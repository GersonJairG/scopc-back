<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'documento', 'correo', 'contrasena','id_estado'//, 'id_rol'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'contrasena', 'remember_token',
    ];

    //relacion uno a muchos con actividad_usuario
    public function actividadesUsuario(){
        return $this->hasMany('App\Models\ActividadUsuario', 'id_usuario');
    }

    //belongsTo de rol
    // public function roles(){
    //     return $this->belongsTo('App\Models\Rol');
    // }

     //belongsTo de estado
    public function estados(){
        return $this->belongsTo('App\Models\Estado');
    }
}
