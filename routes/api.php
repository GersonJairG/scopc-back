<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PATCH, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin");


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Al probar en postman, recordar que los nombres de las variables seran tal cual como se las pasamos en el Controlador.
//actividades
Route::prefix('actividades')->group(function () {
    Route::get('/', 'ActividadController@index');//listo
    Route::post('/new', 'ActividadController@store');//listo(Parametros en body)
    Route::get('/{id}', 'ActividadController@show');//listo
    Route::patch('/{id}', 'ActividadController@update');//listo(id escrito en link, y parametros en params)
    Route::delete('/{id}', 'ActividadController@destroy');//listo
});

//contratos
Route::prefix('contratos')->group(function () {
    Route::get('/', 'ContratoController@index');//listo
    Route::post('/new', 'ContratoController@store');//listo(Parametros en body)
    Route::get('/{id}', 'ContratoController@show');//listo
    Route::patch('/{id}', 'ContratoController@update');//listo(id escrito en link, y parametros en params)
    Route::delete('/{id}', 'ContratoController@destroy');//listo
});

//proyectos
Route::prefix('proyectos')->group(function () {
    Route::get('/', 'ProyectoController@index');//listo
    Route::post('/new', 'ProyectoController@store');//listo(Parametros en body)
    Route::get('/{id}', 'ProyectoController@show');//listo
    Route::patch('/{id}', 'ProyectoController@update');//listo(id escrito en link, y parametros en params)
    Route::delete('/{id}', 'ProyectoController@destroy');//listo
});


