<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ContratoTableSeeder::class);//Listo
        $this->call(RolTableSeeder::class);
        $this->call(PermisoTableSeeder::class);
        $this->call(RolPermisoTableSeeder::class);
        $this->call(TipoManoDeObraTableSeeder::class);
        $this->call(TipoActividadTableSeeder::class);
        $this->call(TipoMaquinariaTableSeeder::class);
        $this->call(TipoMaterialTableSeeder::class);
        $this->call(EstadoTableSeeder::class);
        //$this->call(UsuarioTableSeeder::class);
        //$this->call(ProyectoTableSeeder::class);
        //$this->call(ActividadTableSeeder::class);
        //$this->call(ActividadUsuarioTableSeeder::class);
        //$this->call(RecursoManoDeObraTableSeeder::class);
        //$this->call(RecursoMaquinariaTableSeeder::class);
        //$this->call(RecursoMaterialTableSeeder::class);
        
        
    }
}
    //Semillas de la tabla contrato

class ContratoTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('contrato')->delete();

        $contratos =
        [
            ['id' => 1 , 'nombre' => 'Prestacion' , 'descripcion' => 'En el contrato de servicios se caracteriza por tener objeto que se debe desarrollar o un servicio que se debe prestar, y en el cual el contratista tiene cierta libertad para ejecutarlo por cuanto no está sometido a la continuada y completa subordinación, aunque se precisa que la subordinación también es un elemento presente en el contrato de servicios, pero sin la connotación y sin el alcance que tiene en un contrato de trabajo.']
        ];

        foreach ($contratos as $contrato) {
            DB::table('contrato')->insert($contrato);
        }

    }

}

class RolTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('rol')->delete();

        $roles =
        [
            ['id' => 1 , 'nombre' => 'Administrador' , 'descripcion' => 'Tendra acceso a editar...'],
            ['id' => 2 , 'nombre' => 'Project Manager' , 'descripcion' => 'Tendra acceso a editar...'],
            ['id' => 3 , 'nombre' => 'Colaborador' , 'descripcion' => 'Tendra acceso a visualizar...']
        ];

        foreach ($roles as $rol) {
            DB::table('rol')->insert($rol);
        }

    }

}

class TipoActividadTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('tipo_actividad')->delete();

        $tiposActividades =
        [
            ['id' => 1 , 'nombre' => 'ACTIVIDADES PRELIMNARES' , 'descripcion' => ''],
            ['id' => 2 , 'nombre' => 'PAÑETE-REVOQUES-REPELLOS' , 'descripcion' => ''],
            ['id' => 3 , 'nombre' => 'ESTRUCTURAS EN CONCRETO' , 'descripcion' => ''],
            ['id' => 4 , 'nombre' => 'INSTALACION ELÉCTRICA' , 'descripcion' => ''],
            ['id' => 5 , 'nombre' => 'INSTALACION SANITARIA' , 'descripcion' => ''],
            ['id' => 6 , 'nombre' => 'MAMPOSTERÍA' , 'descripcion' => ''],
            ['id' => 7 , 'nombre' => 'DESAGUES E INSTALACIONES SUBTERRÁNEAS' , 'descripcion' => ''],
            ['id' => 8 , 'nombre' => 'PINTURA' , 'descripcion' => ''],
            ['id' => 9 , 'nombre' => 'VIAS' , 'descripcion' => '']
            
              
            
        ];

        foreach ($tiposActividades as $tipoActividad) {
            DB::table('tipo_actividad')->insert($tipoActividad);
        }

    }
}

class PermisoTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        DB::table('permiso')->delete();

        $permisos =
        [
            ['id' => 1 , 'nombre' => 'Registrar proyectos' , 'descripcion' => ''],
            ['id' => 2 , 'nombre' => 'Actualizar proyectos' , 'descripcion' => ''],
            ['id' => 3 , 'nombre' => 'Consultar proyectos' , 'descripcion' => ''],
            ['id' => 4 , 'nombre' => 'Eliminar proyectos' , 'descripcion' => ''],
            ['id' => 5 , 'nombre' => 'Registrar actividades' , 'descripcion' => ''],
            ['id' => 6 , 'nombre' => 'Actualizar actividades' , 'descripcion' => ''],
            ['id' => 7 , 'nombre' => 'Consultar actividades' , 'descripcion' => ''],
            ['id' => 8 , 'nombre' => 'Eliminar actividades' , 'descripcion' => ''],
            ['id' => 9 , 'nombre' => 'Registrar recursos' , 'descripcion' => ''],
            ['id' => 10 , 'nombre' => 'Actualizar recursos' , 'descripcion' => ''],
            ['id' => 11 , 'nombre' => 'Consultar recursos' , 'descripcion' => ''],
            ['id' => 12 , 'nombre' => 'Eliminar recursos' , 'descripcion' => ''],

        ];

        foreach($permisos as $permiso){
            DB::table('permiso')->insert($permiso);
        }

    }
}

class RolPermisoTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        DB::table('rol_permiso')->delete();

        $rolPermisos =
        [
             ['id' => 1 , 'id_rol' => 1 , 'id_permiso' => 1],//parte de super-admin
             ['id' => 2 , 'id_rol' => 1 , 'id_permiso' => 2],
             ['id' => 3 , 'id_rol' => 1 , 'id_permiso' => 3],
             ['id' => 4 , 'id_rol' => 1 , 'id_permiso' => 4],
             ['id' => 5 , 'id_rol' => 1 , 'id_permiso' => 5],
             ['id' => 6 , 'id_rol' => 1 , 'id_permiso' => 6],
             ['id' => 7 , 'id_rol' => 1 , 'id_permiso' => 7],
             ['id' => 8 , 'id_rol' => 1 , 'id_permiso' => 8],
             ['id' => 9 , 'id_rol' => 1 , 'id_permiso' => 9],
             ['id' => 10 , 'id_rol' => 1 , 'id_permiso' => 10],
             ['id' => 11 , 'id_rol' => 1 , 'id_permiso' => 11], 
             ['id' => 12 , 'id_rol' => 1 , 'id_permiso' => 12],

             /*['id' =>  , 'id_rol' => 2 , 'id_permiso' => 1],//parte de project manager
             ['id' =>  , 'id_rol' => 3 , 'id_permiso' => 1],//parte de colaborador*/

        ];

        foreach($rolPermisos as $rolPermiso){
            DB::table('rol_permiso')->insert($rolPermiso);
        }

    }
}

class TipoMaquinariaTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('tipo_maquinaria')->delete();

        $tiposMaquinaria =
        [
            ['id' => 1 , 'nombre' => 'MAQUINARIA PESADA' , 'descripcion' => ''],
            ['id' => 2 , 'nombre' => 'MAQUINARIA SEMIPESADA' , 'descripcion' => ''],
            ['id' => 3 , 'nombre' => 'EQUIPOS LIGEROS' , 'descripcion' => '']
                    
        ];

        foreach ($tiposMaquinaria as $tipoMaquinaria) {
            DB::table('tipo_maquinaria')->insert($tipoMaquinaria);
        }

    }
}

class TipoMaterialTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('tipo_material')->delete();

        $tiposMaterial =
        [
            ['id' => 1 , 'nombre' => 'LADRILLOS Y BLOQUES' , 'descripcion' => ''],
            ['id' => 2 , 'nombre' => 'ALIGERANTES' , 'descripcion' => ''],
            ['id' => 3 , 'nombre' => 'ACELERANTES' , 'descripcion' => '']
                    
        ];

        foreach ($tiposMaterial as $tipoMaterial) {
            DB::table('tipo_material')->insert($tipoMaterial);
        }

    }
}

class TipoManoDeObraTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('tipo_mano_de_obra')->delete();

        $tiposManoDeObra =
        [
            /*['id' => 1 , 'nombre' => 'CUADRILLA HIDROSANITARIA' , 'descripcion' => '1 OFICIAL, 1 AYUDANTE'],
            ['id' => 2 , 'nombre' => 'CUADRILLA DE CIMENTACIÓN' , 'descripcion' => '1 OFICIAL, 1 AYUDANTE'],
            ['id' => 3 , 'nombre' => 'CUADRILLA DE PINTURA' , 'descripcion' => '1 ESTUCADOR, 1 REMATADOR DE OBRA, 1 AYUDANTE, 1 FACHADAS'],
            ['id' => 4 , 'nombre' => 'CUADRILLA DE ESTRUCTURAS' , 'descripcion' => '1 DIRECTOR, OFICIALES, AYUDANTES, MAESTROS'],
            ['id' => 5 , 'nombre' => 'CUADRILLA ELÉCTRICA' , 'descripcion' => '1 JEFE DE OBRA, 1 OFICIAL 1 AYUDANTE'],
            ['id' => 6 , 'nombre' => 'CUADRILLA DE CARPINTERÍA' , 'descripcion' => '1 CARPINTERO, 1 AYUDANTE PRÁCTICO 1 AYUDANTE'],
            ['id' => 7 , 'nombre' => 'CUADRILLA DE MAMPOSTERÍA' , 'descripcion' => '1 OFICIAL, 1 AYUDANTE'],
            ['id' => 8 , 'nombre' => 'CUADRILLA DE ACABADOS' , 'descripcion' => '1 MAESTRO, 1 RESIDENTE'],
            ['id' => 9 , 'nombre' => 'SUBCUADRILLA DE ENCHAPADORES' , 'descripcion' => '1 ENCHAPADOR CERÁMICA, 1 ENCHAPADOR PORCELANATO, 1 AYUDANTE'],
            ['id' => 10 , 'nombre' => 'SUBCUADRILLA DE INSTALACIÓN DE APARATOS SANITARIOS' , 'descripcion' => '1 OFICIAL'],
            ['id' => 11 , 'nombre' => 'SUBCUADRILLA DE DRYWALL' , 'descripcion' => '1 OFICIAL, 1 AYUDANTE'],
            ['id' => 12 , 'nombre' => 'CUADRILLA DE ASEO' , 'descripcion' => '1 MUJER, 1 HOMBRE']*/
            

                    
        ];

        foreach ($tiposManoDeObra as $tipoManoDeObra) {
            DB::table('tipo_mano_de_obra')->insert($tipoManoDeObra);
        }

    }
}

class EstadoTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('estado')->delete();

        $estados =
        [
            ['id' => 1 , 'nombre' => 'Activo' , 'descripcion' => 'Usuario con password y contraseña'],
            ['id' => 2 , 'nombre' => 'Inactivo' , 'descripcion' => 'Usuario sin password y contraseña']
        ];
        foreach ($estados as $estado) {
            DB::table('estado')->insert($estado);
        }

    }
}



