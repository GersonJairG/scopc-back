<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RecursoMaquinariaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recurso_maquinaria', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');						
			$table->float('precio');
			$table->integer('id_actividad')->unsigned();
			$table->integer('id_tipo_maquinaria')->unsigned();
			$table->timestamps();

			//relacion con actividad
			$table->foreign('id_actividad')->references('id')->on('actividad')->onDelete('cascade');
			//relacion con tipo_maquinaria
			$table->foreign('id_tipo_maquinaria')->references('id')->on('tipo_maquinaria')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('recurso_maquinaria');
	}

}
