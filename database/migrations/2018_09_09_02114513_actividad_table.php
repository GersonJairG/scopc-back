<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ActividadTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actividad', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
			$table->string('unidad');
			$table->string('precio');
			$table->integer('id_tipo_actividad')->unsigned();
			$table->integer('id_proyecto')->unsigned();
			$table->timestamps();

			//relacion con tipo_actividad
			$table->foreign('id_tipo_actividad')->references('id')->on('tipo_actividad')->onDelete('cascade');
			//relacion con proyecto
			$table->foreign('id_proyecto')->references('id')->on('proyecto')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('actividad');
	}

}
