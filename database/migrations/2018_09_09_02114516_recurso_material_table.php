<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RecursoMaterialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recurso_material', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');			
			$table->string('medidas');
			$table->float('peso');
			$table->float('precio');
			$table->integer('id_actividad')->unsigned();
			$table->integer('id_tipo_material')->unsigned();
			$table->timestamps();

			//relacion con actividad
			$table->foreign('id_actividad')->references('id')->on('actividad')->onDelete('cascade');
			//relacion con tipo_material
			$table->foreign('id_tipo_material')->references('id')->on('tipo_material')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('recurso_material');
	}

}
