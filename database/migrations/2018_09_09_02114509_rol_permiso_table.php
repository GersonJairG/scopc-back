<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RolPermisoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rol_permiso', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_rol')->unsigned();
			$table->integer('id_permiso')->unsigned();
			$table->timestamps();

			//relacion con rol
			$table->foreign('id_rol')->references('id')->on('rol')->onDelete('cascade');
			//relacion con permiso
			$table->foreign('id_permiso')->references('id')->on('permiso')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('rol_permiso');
	}

}
