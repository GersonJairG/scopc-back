<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ProyectoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proyecto', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
			$table->timestamp('fecha_inicio');
			$table->string('departamento');
			$table->string('ciudad');
			$table->string('avance_parcial');	
			$table->integer('id_contrato')->unsigned();
			$table->timestamps();
			
			//relacion con contrato
			$table->foreign('id_contrato')->references('id')->on('contrato')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('proyecto');
	}

}
