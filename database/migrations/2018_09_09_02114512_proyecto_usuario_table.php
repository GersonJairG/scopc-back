<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ProyectoUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proyecto_usuario', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_proyecto')->unsigned();
			$table->integer('id_usuario')->unsigned();
			$table->integer('id_rol')->unsigned();
			$table->timestamps();

			//relacion con proyecto
			$table->foreign('id_proyecto')->references('id')->on('proyecto')->onDelete('cascade');
			//relacion con usuario
			$table->foreign('id_usuario')->references('id')->on('usuario')->onDelete('cascade');
			//relacion con rol
			$table->foreign('id_rol')->references('id')->on('rol')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('proyecto_usuario');
	}

}
