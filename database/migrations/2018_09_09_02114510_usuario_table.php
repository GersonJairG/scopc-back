<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuario', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
			$table->string('documento');
			$table->string('correo',100)->unique();
			$table->string('contrasena');
			//$table->integer('id_rol')->unsigned();
			$table->integer('id_estado')->unsigned();
			$table->rememberToken();
			$table->timestamps();

			//relacion con rol
			//$table->foreign('id_rol')->references('id')->on('rol')->onDelete('cascade');
			//relacion con estado
			$table->foreign('id_estado')->references('id')->on('estado')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('usuario');
	}

}
