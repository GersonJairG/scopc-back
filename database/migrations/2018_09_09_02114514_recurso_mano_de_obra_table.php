<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RecursoManoDeObraTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recurso_mano_de_obra', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('encargado');
			$table->string('funcion');
			$table->string('experiencia');		
			$table->float('salario');
			$table->string('unidad');
			$table->float('rendimiento');			
			$table->integer('id_actividad')->unsigned();
			$table->integer('id_tipo_mano_de_obra')->unsigned();			
			$table->timestamps();

			//relacion con actividad
			$table->foreign('id_actividad')->references('id')->on('actividad')->onDelete('cascade');
			//relacion con tipo_mano_de_obra
			$table->foreign('id_tipo_mano_de_obra')->references('id')->on('tipo_mano_de_obra')->onDelete('cascade');			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('recurso_mano_de_obra');
	}

}
